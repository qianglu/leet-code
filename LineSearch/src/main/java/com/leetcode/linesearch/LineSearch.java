package com.leetcode.linesearch;

/**
 * 线性查找法
 * @ClassName LineSearch
 * @Author QIANGLU
 * @Date 2020/12/8 22:34
 * @Version 1.0
 */
public class LineSearch {

    public static void main(String[] args) {

        int[] data = new int[]{1,3,4,5,6,7};
        int index = search(data,5);
        System.out.println("index: "+index);

    }

    public static int search(int[] data, int target){

        for (int i = 0 ; i <data.length;i++){

            if(target == data[i]){
                return i;
            }
        }

        return -1;
    }
}
