package com.leetcode.selectcode;

/**
 * 选择排序
 *
 * @ClassName SelectSort
 * @Author QIANGLU
 * @Date 2020/12/9 21:52
 * @Version 1.0
 */
public class SelectSort {


    public static void main(String[] args) {

        int[] data = new int[]{4, 2, 5, 1, 6, 8, 9, 3};

        selectSort(data);
        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }
    }


    public static void selectSort(int[] data) {

        if (data.length == 0) {
            return;
        }

        for (int i = 0; i < data.length; i++) {

            int min = i;
            for (int j = i; j < data.length; j++) {

                if ( data[j]  < data[min] ) {
                    min = j;
                }

            }
            int t = data[i];
            data[i] = data[min];
            data[min] = t;
        }

    }
}
